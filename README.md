# Bank Test App

### Instructions

Run `docker-compose up` in project directory. Application will be available at `127.0.0.1:8080`.

### Endpoints

`GET /balance` returns current balance

`POST /deposit` deposits `amount` (passed as POST param) on bank account and returns balance

`POST /withdraw` withdraws `amount` (passed as POST param) from bank account and returns balance


### Tests
Run `docker-compose exec php-fpm /var/www/bank.loc/Tests/run.sh` in project directory.
