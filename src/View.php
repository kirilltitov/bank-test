<?php

declare(strict_types=1);

/**
 * View is a kind of response received by user as a result of his action. It can be HTML page, JSON or even redirect.
 * In this application only JSON view is implemented.
 * View must always represent itself as string (even there is no visible output).
 */
abstract class View
{
    abstract public function getContent(): string;
}
