<?php

declare(strict_types=1);

namespace Controller;

class Balance extends Bank
{
    public function execute(\Request $Request): \View
    {
        return new \View\JSON([
            'result' => true,
            'data' => [ 'balance' => \Logic\Bank::getBalance() ],
        ]);
    }
}
