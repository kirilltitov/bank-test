<?php

declare(strict_types=1);

namespace Controller;

class Withdraw extends Bank
{
    public function executePOST(\Request $Request): \View
    {
        if (empty($Request->POST['amount']))
        {
            throw new \Logic\Bank\Exception\MissingValue('Please specify withdraw amount with POST param \'amount\'');
        }
        $amount = (float)$Request->POST['amount'];
        return new \View\JSON([
            'result' => true,
            'data' => [ 'balance' => \Logic\Bank::withdraw($amount) ],
        ]);
    }
}
