<?php

declare(strict_types=1);

namespace Controller;

class Deposit extends Bank
{
    public function executePOST(\Request $Request): \View
    {
        if (empty($Request->POST['amount']))
        {
            throw new \Logic\Bank\Exception\MissingValue('Please specify deposit amount with POST param \'amount\'');
        }
        $amount = (float)$Request->POST['amount'];
        return new \View\JSON([
            'result' => true,
            'data' => [ 'balance' => \Logic\Bank::deposit($amount) ],
        ]);
    }
}
