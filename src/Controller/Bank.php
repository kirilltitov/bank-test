<?php

declare(strict_types=1);

namespace Controller;

abstract class Bank extends \Controller
{
    public function execute(\Request $Request): \View
    {
        throw new \Logic\Bank\Exception\MissingValue('No action');
    }
}
