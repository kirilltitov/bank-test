<?php

declare(strict_types=1);

abstract class Entity
{
    /**
     * Returns DB names associated with entity fields names
     * @return array
     */
    abstract public function getFields(): array;
    abstract public function getID(): ?int;
    abstract public function setID(int $ID);

    static public function populate(array $from): self
    {
        $Instance = new static;
        $Fields = $Instance->getFields();
        foreach ($from as $name => $value)
        {
            if (!array_key_exists($name, $Fields))
            {
                continue;
            }
            $Instance->setFieldValue($Fields[$name], $value);
        }
        return $Instance;
    }

    public function getFieldValue(string $name)
    {
        $method = 'get' . ucfirst($name);
        if (!method_exists($this, $method))
        {
            return null;
        }
        return $this->{$method}();
    }

    public function setFieldValue(string $name, $value)
    {
        $method = 'set' . ucfirst($name);
        if (!method_exists($this, $method))
        {
            return;
        }
        $this->{$method}($value);
    }
}
