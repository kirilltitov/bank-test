<?php

require_once __DIR__ . '/../Autoload.php';

ini_set('error_log', '/var/log/php-scripts.log');
ini_set('precision', 14);
ini_set('serialize_precision', -1);

$App = new \Application\Bank;

// This is not quite a Singleton in its general meaning. Just a quickest way of storing application instance globally.
// Of course there are a gazillion ways of doing it _properly_ (like DI etc etc), but for now this will work.
Application::$Instance = $App;

// in case we would like to pass fixture for tests
$App->setRequest(new Request($_GET, $_POST, $_SERVER, $_COOKIE));

echo $App->execute();
