<?php

return [
    'db' => [
        'driver' => 'pgsql',
        'host' => 'postgres',
        'port' => 5432,
        'db' => 'bank',
        'username' => 'bank',
        'password' => 'bank',
    ],
    'http' => [
        'allowed_methods' => [ 'GET', 'POST' ],
        'default_method' => 'GET',
    ],
    'host' => 'bank.loc',
    'limitations' => [
        'deposits_per_day' => 4,
        'max_deposit_per_day' => 150 * 1000,
        'max_deposit_per_transaction' => 40 * 1000,
        'withdrawals_per_day' => 3,
        'max_withdrawal_per_day' => 50 * 1000,
        'max_withdrawal_per_transaction' => 20 * 1000,
    ],
];
