<?php

declare(strict_types=1);

namespace Entity;

class Balance extends \Entity
{
    const AMOUNT_MULTIPLIER = 10000;

    /** @var int */
    protected $ID;

    /** @var int */
    protected $balance;

    /**
     * Returns DB names associated with entity fields names
     * @return array
     */
    public function getFields(): array
    {
        return [
            'id' => 'ID',
            'balance' => 'balance'
        ];
    }

    public function getID(): ?int
    {
        return $this->ID;
    }

    public function setID(int $ID)
    {
        $this->ID = $ID;
        return $this;
    }

    /**
     * Returned value is always multiplied
     * @param bool $unmultiplied
     * @return int|float
     */
    public function getBalance(bool $unmultiplied = false)
    {
        $result = $this->balance;
        if ($unmultiplied)
        {
            $result /= static::AMOUNT_MULTIPLIER;
        }
        return $result;
    }

    /**
     * Amount value MUST BE multiplied
     * @param int $balance
     * @return Balance
     */
    public function setBalance(int $balance): Balance
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * Amount value MUST BE unmultiplied
     * @param float $by
     * @return Balance
     */
    public function increase(float $by): Balance
    {
        return $this->setBalance((int)($this->getBalance() + ($by * static::AMOUNT_MULTIPLIER)));
    }

    /**
     * Amount value MUST BE unmultiplied
     * @param float $by
     * @return Balance
     */
    public function decrease(float $by): Balance
    {
        return $this->setBalance((int)($this->getBalance() - ($by * static::AMOUNT_MULTIPLIER)));
    }
}
