<?php

declare(strict_types=1);

namespace Entity\Balance;

class History extends \Entity
{
    const OPERATION_DEPOSIT = 'deposit';
    const OPERATION_WITHDRAWAL = 'withdrawal';
    const ALLOWED_OPERATIONS = [ self::OPERATION_DEPOSIT, self::OPERATION_WITHDRAWAL ];

    /** @var int */
    protected $ID;

    /** @var int */
    protected $balanceID;

    /** @var string */
    protected $operation;

    /** @var int */
    protected $delta;

    /** @var string */
    protected $eventDate;

    /**
     * Returns DB names associated with entity fields names
     * @return array
     */
    public function getFields(): array
    {
        return [
            'id' => 'ID',
            'id_balance' => 'balanceID',
            'operation' => 'operation',
            'delta' => 'delta',
            'dt_event' => 'eventDate'
        ];
    }

    public function getID(): ?int
    {
        return $this->ID;
    }

    public function setID(int $ID)
    {
        $this->ID = $ID;
        return $this;
    }

    /**
     * @return int
     */
    public function getBalanceID(): int
    {
        return $this->balanceID;
    }

    /**
     * @param int $balanceID
     * @return History
     */
    public function setBalanceID(int $balanceID): History
    {
        $this->balanceID = $balanceID;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     * @return History
     * @throws \Exception
     */
    public function setOperation(string $operation): History
    {
        if (!in_array($operation, static::ALLOWED_OPERATIONS))
        {
            throw new \Exception("Invalid operation {$operation}");
        }
        $this->operation = $operation;
        return $this;
    }

    /**
     * Returned value is always multiplied
     * @return int
     */
    public function getDelta(): int
    {
        return $this->delta;
    }

    /**
     * Amount value MUST BE unmultiplied
     * @param float $delta
     * @return History
     */
    public function setDelta(float $delta): History
    {
        $this->delta = (int)($delta * \Entity\Balance::AMOUNT_MULTIPLIER);
        return $this;
    }

    /**
     * @return string
     */
    public function getEventDate(): string
    {
        return $this->eventDate;
    }

    /**
     * @param string $eventDate
     * @return History
     */
    public function setEventDate(string $eventDate): History
    {
        $this->eventDate = $eventDate;
        return $this;
    }

    public function setEventDateNow(): History
    {
        return $this->setEventDate((new \DateTime)->format(\DATE_W3C));
    }
}
