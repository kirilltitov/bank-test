<?php

declare(strict_types=1);

class Application
{
    const DIR = __DIR__;
    const DEFAULT_ROUTE = 'index';
    const DEFAULT_REQUEST_METHOD = 'GET';

    /** @var PDO */
    protected $DB;

    /** @var array */
    protected $Config;

    /** @var Request */
    protected $Request;

    /** @var Application */
    static public $Instance;

    static public $scheme = 'public';

    public $isDebug = false;

    public function setRequest(Request $Request): self
    {
        $this->Request = $Request;
        return $this;
    }

    public function execute(): string
    {
        [ $Controller, $Leftovers ] = $this->getControllerAndLeftovers();
        $this->Request->LeftoverParams = $Leftovers;
        $Controller->beforeExecute($this->Request);
        $method = $this->getRequestMethod();
        $actionName = 'execute' . ($method === $this->getConfig()['http']['default_method'] ? '' : $method);
        if (!method_exists($Controller, $actionName))
        {
            die("No such method: '" . get_class($Controller) . "::{$actionName}'"); // todo proper 404
        }
        $result = $Controller->{$actionName}($this->Request)->getContent();
        return $result;
    }

    protected function getRequestMethod(): string
    {
        $Config = $this->getConfig()['http'];
        $method = strtoupper($this->Request->Data['REQUEST_METHOD'] ?? $Config['default_method']);
        return in_array($method, $Config['allowed_methods']) ? $method : $Config['default_method'];
    }

    protected function getControllerAndLeftovers(): array {
        $route = $this->Request->Data['REQUEST_URI'] ?? static::DEFAULT_ROUTE;
        $route = explode('?', $route);
        $route = reset($route);
        $route = explode('.', $route);
        $route = reset($route);
        $route = ltrim($route, '/');
        if (!$route)
        {
            $route = static::DEFAULT_ROUTE;
        }
        if ($route[mb_strlen($route) - 1] === '/')
        {
            $route .= static::DEFAULT_ROUTE;
        }
        $Parts = explode('/', $route);
        $Leftovers = [];
        while (true)
        {
            if (empty($Parts))
            {
                throw new \Exception('Error 404');
            }
            $controllerName = 'Controller\\' . implode('\\', array_map('ucfirst', $Parts));
            if (!class_exists($controllerName))
            {
                $Leftovers[] = array_pop($Parts);
                continue;
            }
            return [ new $controllerName, $Leftovers ];
        }
    }

    public function getConfig(): array
    {
        if ($this->Config !== null)
        {
            return $this->Config;
        }
        return $this->Config = require __DIR__ . '/Config.php';
    }

    /**
     * @return PDO
     * @throws Exception
     */
    public function getDB(): PDO {
        if ($this->DB !== null)
        {
            return $this->DB;
        }
        $Config = $this->getConfig()['db'];
        $dsn = sprintf(
            '%s:dbname=%s;host=%s;port=%d',
            strtolower($Config['driver']),
            $Config['db'],
            $Config['host'],
            $Config['port']
        );
        try
        {
            $this->DB = new \PDO($dsn, $Config['username'], $Config['password']);
        }
        catch (\PDOException $e)
        {
            throw new \Exception("Could not connect to PgSQL server with credentials '{$dsn}'");
        }
        return $this->DB;
    }
}
