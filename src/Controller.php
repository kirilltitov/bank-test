<?php

declare(strict_types=1);

abstract class Controller
{
    abstract public function execute(\Request $Request): View;

    public function beforeExecute(\Request $Request)
    {
        // noop
    }
}
