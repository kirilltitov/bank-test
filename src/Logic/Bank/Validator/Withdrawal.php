<?php

declare(strict_types=1);

namespace Logic\Bank\Validator;

use Entity\Balance;
use Logic\Bank\Exception\Withdrawal\{
    InsufficientBalance,
    LimitAmountPerDayExceeded,
    LimitAmountPerTransactionExceeded,
    LimitTransactionsPerDayExceeded
};

class Withdrawal extends \Logic\Bank\Validator
{
    static public function validate(int $ID, float $amount)
    {
        $StatsForToday = (new \DBTable\Balance\History)->getWithdrawalStatsForToday($ID);
        static::validatePerTransaction($amount);
        static::validateTransactionsPerDay($StatsForToday);
        static::validateMaxWithdrawalPerDay($amount, $StatsForToday);
        static::validateSufficientBalance(\Logic\Bank::getBalanceEntity($ID), $amount);
    }

    static protected function validatePerTransaction(float $amount)
    {
        $maximumAmountPerTransaction = static::getConfig()['max_withdrawal_per_transaction'];
        if ($amount > $maximumAmountPerTransaction)
        {
            $overlap = $amount - $maximumAmountPerTransaction;
            throw new LimitAmountPerTransactionExceeded(
                "Maximum allowed withdraw amount per transaction {$maximumAmountPerTransaction} exceeded (overlap: {$overlap})"
            );
        }
    }

    static protected function validateMaxWithdrawalPerDay(float $amount, array $StatsForToday)
    {
        $maximumAmountPerDay = static::getConfig()['max_withdrawal_per_day'] * Balance::AMOUNT_MULTIPLIER;
        if ((int)$StatsForToday['total'] + ($amount * Balance::AMOUNT_MULTIPLIER) > $maximumAmountPerDay)
        {
            $overlap = (0
                + (int)$StatsForToday['total']
                + ($amount * Balance::AMOUNT_MULTIPLIER)
                - $maximumAmountPerDay
            ) / Balance::AMOUNT_MULTIPLIER;
            throw new LimitAmountPerDayExceeded(
                "Maximum allowed withdraw amount per day {$maximumAmountPerDay} exceeded (overlap: {$overlap})"
            );
        }
    }

    static protected function validateTransactionsPerDay(array $StatsForToday)
    {
        $maximumTransactionsPerDay = static::getConfig()['withdrawals_per_day'];
        if ((int)$StatsForToday['cnt'] >= $maximumTransactionsPerDay)
        {
            throw new LimitTransactionsPerDayExceeded(
                "Maximum allowed withdrawal transactions number per day of {$maximumTransactionsPerDay} exceeded"
            );
        }
    }

    static protected function validateSufficientBalance(Balance $Balance, float $amount)
    {
        if ($amount > $Balance->getBalance(true))
        {
            $lacking = number_format(
                $amount - $Balance->getBalance(true),
                2,
                '.',
                ''
            );
            throw new InsufficientBalance(
                "Insufficient balance (balance: {$Balance->getBalance(true)}, lacking: {$lacking})"
            );
        }
    }
}
