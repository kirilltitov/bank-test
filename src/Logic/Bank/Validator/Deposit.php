<?php

declare(strict_types=1);

namespace Logic\Bank\Validator;

use Entity\Balance;
use Logic\Bank\Exception\Deposit\{
    LimitAmountPerTransactionExceeded,
    LimitAmountPerDayExceeded,
    LimitTransactionsPerDayExceeded
};

class Deposit extends \Logic\Bank\Validator
{
    static public function validate(int $ID, float $amount)
    {
        $StatsForToday = (new \DBTable\Balance\History)->getDepositStatsForToday($ID);
        static::validatePerTransaction($amount);
        static::validateTransactionsPerDay($StatsForToday);
        static::validateMaxDepositPerDay($amount, $StatsForToday);
    }

    static protected function validatePerTransaction(float $amount)
    {
        $maximumAmountPerTransaction = static::getConfig()['max_deposit_per_transaction'];
        if ($amount > $maximumAmountPerTransaction)
        {
            $overlap = $amount - $maximumAmountPerTransaction;
            throw new LimitAmountPerTransactionExceeded(
                "Maximum allowed deposit amount per transaction {$maximumAmountPerTransaction} exceeded (overlap: {$overlap})"
            );
        }
    }

    static protected function validateMaxDepositPerDay(float $amount, array $StatsForToday)
    {
        $maximumAmountPerDay = static::getConfig()['max_deposit_per_day'] * Balance::AMOUNT_MULTIPLIER;
        if ((int)$StatsForToday['total'] + ($amount * Balance::AMOUNT_MULTIPLIER) > $maximumAmountPerDay)
        {
            $overlap = (0
                + (int)$StatsForToday['total']
                + ($amount * Balance::AMOUNT_MULTIPLIER)
                - $maximumAmountPerDay
            ) / Balance::AMOUNT_MULTIPLIER;
            throw new LimitAmountPerDayExceeded(
                "Maximum allowed deposit amount per day {$maximumAmountPerDay} exceeded (overlap: {$overlap})"
            );
        }
    }

    static protected function validateTransactionsPerDay(array $StatsForToday)
    {
        $maximumTransactionsPerDay = static::getConfig()['deposits_per_day'];
        if ((int)$StatsForToday['cnt'] >= $maximumTransactionsPerDay)
        {
            throw new LimitTransactionsPerDayExceeded(
                "Maximum allowed deposit transactions number per day of {$maximumTransactionsPerDay} exceeded"
            );
        }
    }
}
