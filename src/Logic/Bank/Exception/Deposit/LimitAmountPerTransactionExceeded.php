<?php

declare(strict_types=1);

namespace Logic\Bank\Exception\Deposit;

class LimitAmountPerTransactionExceeded extends \Logic\Bank\Exception\Deposit
{
    protected $code = 413;
}
