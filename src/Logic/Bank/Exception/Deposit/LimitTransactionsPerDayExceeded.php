<?php

declare(strict_types=1);

namespace Logic\Bank\Exception\Deposit;

class LimitTransactionsPerDayExceeded extends \Logic\Bank\Exception\Deposit
{
    protected $code = 429;
}
