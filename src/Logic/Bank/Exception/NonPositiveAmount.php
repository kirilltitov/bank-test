<?php

declare(strict_types=1);

namespace Logic\Bank\Exception;

class NonPositiveAmount extends \Logic\Bank\Exception
{
    protected $code = 406;
    protected $message = 'The amount must be positive';
}
