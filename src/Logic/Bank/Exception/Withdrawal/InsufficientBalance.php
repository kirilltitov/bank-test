<?php

declare(strict_types=1);

namespace Logic\Bank\Exception\Withdrawal;

class InsufficientBalance extends \Logic\Bank\Exception\Withdrawal
{

}
