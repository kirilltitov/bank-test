<?php

declare(strict_types=1);

namespace Logic\Bank\Exception\Withdrawal;

class LimitTransactionsPerDayExceeded extends \Logic\Bank\Exception\Withdrawal
{
    protected $code = 429;
}
