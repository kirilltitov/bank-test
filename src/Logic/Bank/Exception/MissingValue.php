<?php

declare(strict_types=1);

namespace Logic\Bank\Exception;

class MissingValue extends \Logic\Bank\Exception
{
    protected $code = 400;
}
