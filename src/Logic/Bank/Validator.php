<?php

declare(strict_types=1);

namespace Logic\Bank;

class Validator
{
    static protected function getConfig(): array
    {
        return \Application::$Instance->getConfig()['limitations'];
    }
}
