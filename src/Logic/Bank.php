<?php

declare(strict_types=1);

namespace Logic;

use DBTable\Balance as BalanceTable;
use DBTable\Balance\History;
use \Logic\Bank\Validator;

class Bank
{
    const ID_BALANCE = 1;

    static public function getBalance(bool $unmultiplied = true): float
    {
        return (new BalanceTable())->getBalance(static::ID_BALANCE, $unmultiplied);
    }

    static public function deposit(float $amount): float
    {
        static::validatePositiveAmount($amount);
        $HistoryTable = new History;
        return $HistoryTable->withTransactionalRetry(function() use ($amount, $HistoryTable) {
            $Balance = static::getBalanceEntity(static::ID_BALANCE, true);
            Validator\Deposit::validate(static::ID_BALANCE, $amount);
            $Balance->increase($amount);
            (new BalanceTable())->update($Balance);
            $HistoryTable->logDeposit(static::ID_BALANCE, $amount);
            return $Balance->getBalance(true);
        });
    }

    static public function withdraw(float $amount): float
    {
        static::validatePositiveAmount($amount);
        $HistoryTable = new History;
        return $HistoryTable->withTransactionalRetry(function() use ($amount, $HistoryTable) {
            $Balance = static::getBalanceEntity(static::ID_BALANCE, true);
            Validator\Withdrawal::validate(static::ID_BALANCE, $amount);
            $Balance->decrease($amount);
            (new BalanceTable())->update($Balance);
            $HistoryTable->logWithdrawal(static::ID_BALANCE, $amount);
            return $Balance->getBalance(true);
        });
    }

    static public function getBalanceEntity(int $ID, bool $forUpdate = false): \Entity\Balance
    {
        /** @var \Entity\Balance $Result */
        $Result = (new BalanceTable())->loadByID($ID, $forUpdate);
        return $Result;
    }

    static protected function validatePositiveAmount(float $amount)
    {
        if ($amount <= 0)
        {
            throw new \Logic\Bank\Exception\NonPositiveAmount;
        }
    }

    static protected function getConfig(): array
    {
        return \Application::$Instance->getConfig()['limitations'];
    }
}
