<?php

declare(strict_types=1);

namespace Application;

class Bank extends \Application
{
    public function execute(): string
    {
        try
        {
            $result = parent::execute();
        }
        catch (\Logic\Bank\Exception $E)
        {
            $result = (new \View\JSON([
                'result' => false,
                'error' => [
                    'code' => $E->getCode(),
                    'description' => $E->getMessage(),
                ],
            ]))->getContent();
        }
        catch (\Exception $E)
        {
            error_log("Internal Server Error: {$E->getMessage()} ({$E->getCode()})");
            $result = (new \View\JSON([
                'result' => false,
                'error' => [
                    'code' => 500,
                    'description' => 'Internal Server Error',
                ],
            ]))->getContent();
        }
        return $result;
    }
}
