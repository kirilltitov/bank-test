<?php

declare(strict_types=1);

namespace View;

class JSON extends \View
{
    /** @var array */
    protected $Result;

    public function __construct(array $Result = [])
    {
        $this->Result = $Result;
    }

    public function getContent(): string
    {
        header('Content-Type: application/json');
        return json_encode($this->Result);
    }
}
