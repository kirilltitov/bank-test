<?php

declare(strict_types=1);

abstract class DBTable
{
    const ORDER_ASC = 'ASC';
    const ORDER_DESC = 'DESC';
    const ORDER_DEFAULT = self::ORDER_ASC;
    const ALLOWED_ORDERS = [ self::ORDER_ASC, self::ORDER_DESC ];
    const DEFAULT_LIMIT = 100;

    abstract public function getTableName(): string;
    abstract public function getEntityClassName(): string;

    protected function getPreparedTableName(): string
    {
        return implode(
            '.',
            array_map(
                function($item) { return '"' . $item . '"'; },
                explode('.', $this->getTableName())
            )
        );
    }

    public function loadAll(
        int $limit = self::DEFAULT_LIMIT,
        int $offset = null,
        string $orderField = 'id',
        string $orderDirection = self::ORDER_DEFAULT
    ): array
    {
        $orderDirection = strtoupper($orderDirection);
        if (!in_array($orderDirection, static::ALLOWED_ORDERS))
        {
            $orderDirection = static::ORDER_DEFAULT;
        }
        $query = "
            {$this->getBaseSelectPart()}
            {$this->getOrderPart($orderField, $orderDirection)}
            {$this->getLimitPart($limit, $offset)}
        ";
        return $this->executeAndGetPopulatedEntities($query);
    }

    protected function executeAndGetPopulatedEntities(string $query, array $Params = []): array
    {
        $entityClassName = $this->getEntityClassName();
        if (!class_exists($entityClassName))
        {
            throw new \Exception("Entity class '{$entityClassName}' does not exist!");
        }
        $Result = [];
        foreach ($this->executeQuery($query, $Params) as $Row)
        {
            /** @var \Entity $Instance */
            $Result[] = call_user_func([ $entityClassName, 'populate' ], $Row);
        }
        return $Result;
    }

    protected function getBaseSelectPart(): string
    {
        return "SELECT * FROM {$this->getPreparedTableName()}";
    }

    protected function getWherePart($Critery): string
    {
        $CriteryList = array_map(
            function($key, $value) { return "\"{$key}\" = {$value}"; },
            array_keys($Critery),
            $this->getPlaceholders(array_keys($Critery))
        );
        return "WHERE " . implode(' AND ', $CriteryList);
    }

    protected function getOrderPart(string $orderField, string $orderDirection): string
    {
        $orderDirection = strtoupper($orderDirection);
        if (!in_array($orderDirection, static::ALLOWED_ORDERS))
        {
            $orderDirection = static::ORDER_DEFAULT;
        }
        return "ORDER BY \"{$orderField}\" {$orderDirection}";
    }

    protected function getLimitPart(int $limit, ?int $offset): string
    {
        $result = "LIMIT {$limit}";
        if ($offset !== null)
        {
            $result .= " OFFSET {$offset}";
        }
        return $result;
    }

    protected function getPlaceholders(array $Fields): array
    {
        return array_map(function($name) { return ":{$name}"; }, $Fields);
    }

    public function loadBy(
        array $Critery,
        int $limit = self::DEFAULT_LIMIT,
        int $offset = null,
        string $orderField = 'id',
        string $orderDirection = self::ORDER_DEFAULT,
        bool $forUpdate = false
    )
    {
        $entityClassName = $this->getEntityClassName();
        if (!class_exists($entityClassName))
        {
            throw new \Exception("Entity class '{$entityClassName}' does not exist!");
        }
        /** @var Entity $Entity */
        $Entity = new $entityClassName;
        $Fields = $Entity->getFields();
        $Critery = array_filter(
            $Critery,
            function($item) use ($Fields) { return array_key_exists($item, $Fields); },
            ARRAY_FILTER_USE_KEY
        );
        $query = "
            {$this->getBaseSelectPart()}
            {$this->getWherePart($Critery)}
            {$this->getOrderPart($orderField, $orderDirection)}
            {$this->getLimitPart($limit, $offset)}
            " . ($forUpdate ? 'FOR UPDATE' : '') . "
        ";
        $Result = $this->executeAndGetPopulatedEntities($query, $Critery);
        if ($limit === 1)
        {
            $Result = reset($Result);
        }
        return $Result;
    }

    public function insert(\Entity $Entity)
    {
        $Fields = $Entity->getFields();
        if ($Entity->getID() === null)
        {
            unset($Fields['id']);
        }
        $Prepared = array_combine(
            $this->getPlaceholders(array_keys($Fields)),
            array_map([ $Entity, 'getFieldValue' ], $Fields)
        );
        $Result = $this->executeQuery(
            "
                INSERT INTO {$this->getPreparedTableName()}
                (" . implode(', ', array_keys($Fields)) . ")
                VALUES
                (" . implode(', ', array_keys($Prepared)) . ")
                RETURNING \"id\"
            ",
            $Prepared
        );
        $Entity->setID(reset($Result)['id']);
    }

    public function update(\Entity $Entity)
    {
        if ($Entity->getID() === null)
        {
            throw new Exception('ID field must not be null');
        }
        $Fields = $Entity->getFields();
        $Placeholders = $this->getPlaceholders(array_keys($Fields));
        $UpdatePart = array_map(
            function($name, $placeholder) { return "{$name} = {$placeholder}"; },
            array_keys($Fields),
            $Placeholders
        );
        $Critery = [ 'id' => $Entity->getID() ];
        $this->executeQuery(
            "
                UPDATE {$this->getPreparedTableName()}
                SET " . implode(', ', $UpdatePart) . "
                {$this->getWherePart($Critery)}
            ",
            $Prepared = array_combine(
                $this->getPlaceholders(array_keys($Fields)),
                array_map([ $Entity, 'getFieldValue' ], $Fields)
            )
        );
    }

    public function executeQuery(string $query, array $Params = []): array
    {
        $PreparedQuery = $this->getDB()->prepare($query);
        $start = microtime(true);
        $PreparedQuery->execute($Params);
        $stop = microtime(true) - $start;
        $query = preg_replace('/\s+/', ' ', trim($query));
        if (\Application::$Instance->isDebug)
        {
            error_log("QUERY(" . number_format($stop, 4) . "s, {$PreparedQuery->rowCount()} results): {$query}");
        }
        return $PreparedQuery->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getDB(): \PDO
    {
        return \Application::$Instance->getDB();
    }
}
