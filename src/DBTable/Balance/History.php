<?php

declare(strict_types=1);

namespace DBTable\Balance;

use Entity\Balance\History as EntityHistory;

class History extends \DBTable
{
    public function getTableName(): string
    {
        return \Application::$scheme . '.balance_history';
    }

    public function getEntityClassName(): string
    {
        return EntityHistory::class;
    }

    public function logDeposit(int $balanceID, float $amount)
    {
        $this->log(EntityHistory::OPERATION_DEPOSIT, $balanceID, $amount);
    }

    public function logWithdrawal(int $balanceID, float $amount)
    {
        $this->log(EntityHistory::OPERATION_WITHDRAWAL, $balanceID, $amount);
    }

    protected function log(string $operation, int $balanceID, float $amount)
    {
        $className = $this->getEntityClassName();
        /** @var EntityHistory $Entity */
        $Entity = new $className;
        $Entity
            ->setBalanceID($balanceID)
            ->setOperation($operation)
            ->setDelta($amount)
            ->setEventDateNow();
        $this->insert($Entity);
    }

    public function getDepositStatsForToday(int $ID): array
    {
        return $this->getStatsForToday($ID, EntityHistory::OPERATION_DEPOSIT);
    }

    public function getWithdrawalStatsForToday(int $ID): array
    {
        return $this->getStatsForToday($ID, EntityHistory::OPERATION_WITHDRAWAL);
    }

    public function getStatsForToday(int $ID, string $operation): array
    {
        return $this->executeQuery("
            SELECT
                COALESCE(SUM(delta), 0) AS total,
                COUNT(DISTINCT id) AS cnt
            FROM {$this->getTableName()}
            WHERE 1 = 1
                AND id_balance = :id
                AND operation = :operation
                AND dt_event::date = now()::date
        ", [ 'id' => $ID, 'operation' => $operation ])[0];
    }

    public function withTransactionalRetry(callable $closure, int $tries = 100)
    {
        while (--$tries > 0)
        {
            try
            {
                $this->begin();
                $result = $closure();
                $this->commit();
                return $result;
                break;
            }
            catch (\Exception $E)
            {
                if (strpos($E->getMessage(), 'The transaction might succeed if retried') !== false)
                {
                    $this->rollback();
                    continue;
                }
                $this->rollback();
                throw $E;
            }
        }
    }

    public function begin()
    {
        $this->getDB()->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->executeQuery('BEGIN TRANSACTION ISOLATION LEVEL SERIALIZABLE');
    }

    public function commit()
    {
        $this->executeQuery('COMMIT');
    }

    public function rollback()
    {
        $this->executeQuery('ROLLBACK');
    }
}
