<?php

declare(strict_types=1);

namespace DBTable;

class Balance extends \DBTable
{
    public function getTableName(): string
    {
        return \Application::$scheme . '.balance';
    }

    public function getEntityClassName(): string
    {
        return \Entity\Balance::class;
    }

    public function loadByID(int $ID, bool $forUpdate): \Entity\Balance
    {
        /** @var \Entity\Balance $Entity */
        $Entity = $this->loadBy([ 'id' => $ID ], 1, 0, 'id', 'asc', $forUpdate);
        return $Entity;
    }

    public function getBalance(int $ID, bool $unmultiplied = true): float
    {
        $this->executeQuery('BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ');
        $Entity = $this->loadByID($ID, false);
        $this->executeQuery('COMMIT');
        return $Entity->getBalance($unmultiplied);
    }
}
