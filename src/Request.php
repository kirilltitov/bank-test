<?php

declare(strict_types=1);

class Request
{
    public $GET = [];
    public $POST = [];
    public $Data = [];
    public $Cookies = [];
    public $LeftoverParams = [];

    /**
     * Request constructor.
     * @param array $GET
     * @param array $POST
     * @param array $Data
     * @param array $Cookies
     * @param array $LeftoverParams
     */
    public function __construct(
        array $GET = [],
        array $POST = [],
        array $Data = [],
        array $Cookies = [],
        array $LeftoverParams = []
    )
    {
        $this->GET = $GET;
        $this->POST = $POST;
        $this->Data = $Data;
        $this->Cookies = $Cookies;
        $this->LeftoverParams = $LeftoverParams;
    }
}
