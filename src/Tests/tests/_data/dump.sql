drop schema if exists test cascade;

create schema test;

create table test.balance
(
	id serial not null constraint balance_pkey primary key,
	balance integer not null
);

create unique index balance_id_uindex on test.balance (id);

insert into test.balance (balance) values (0);

create type test.operation as enum('deposit', 'withdrawal');

create table test.balance_history
(
	id serial not null constraint balance_history_pkey primary key,
	id_balance integer not null,
	operation test.operation not null,
	delta integer,
	dt_event timestamp with time zone not null
);

create unique index balance_history_id_uindex on test.balance_history (id);
create index balance_history_id_balance_index on test.balance_history (id_balance);
create index balance_history_operation_index on test.balance_history (operation);
create index balance_history_dt_event_index on test.balance_history (dt_event);
