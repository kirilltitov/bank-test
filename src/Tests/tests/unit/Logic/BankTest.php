<?php

use Logic\Bank;

class BankTest extends Test
{
    public function testValidDeposit()
    {
        $this->assertEquals(0, Bank::getBalance());
        Bank::deposit(13.37);
        $this->assertEquals(13.37, Bank::getBalance());
    }

    /**
     * @expectedException \Logic\Bank\Exception\Deposit\LimitAmountPerTransactionExceeded
     */
    public function testInvalidDepositPerTransaction()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(100500);
    }

    /**
     * @expectedException \Logic\Bank\Exception\Deposit\LimitTransactionsPerDayExceeded
     */
    public function testInvalidDepositAmountPerDay()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(10);
        Bank::deposit(10);
        Bank::deposit(10);
        Bank::deposit(10);
        Bank::deposit(10);
    }

    /**
     * @expectedException \Logic\Bank\Exception\Deposit\LimitAmountPerDayExceeded
     */
    public function testInvalidDepositTransactionsPerDay()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(40000);
        Bank::deposit(40000);
        Bank::deposit(40000);
        Bank::deposit(40000);
    }

    /**
     * @expectedException \Logic\Bank\Exception\NonPositiveAmount
     */
    public function testNegativeDepositAmount()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(-1);
    }

    /**
     * @expectedException \Logic\Bank\Exception\NonPositiveAmount
     */
    public function testZeroDepositAmount()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(0);
    }

    public function testValidWithdrawal()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(13.37);
        $this->assertEquals(13.37, Bank::getBalance());
        Bank::withdraw(3.22);
        $this->assertEquals(10.15, Bank::getBalance());
    }

    /**
     * @expectedException \Logic\Bank\Exception\Withdrawal\LimitAmountPerTransactionExceeded
     */
    public function testInvalidWithdrawalPerTransaction()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(40000);
        Bank::withdraw(30000);
    }

    /**
     * @expectedException \Logic\Bank\Exception\Withdrawal\LimitTransactionsPerDayExceeded
     */
    public function testInvalidWithdrawalAmountPerDay()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(40000);

        Bank::withdraw(10);
        Bank::withdraw(10);
        Bank::withdraw(10);
        Bank::withdraw(10);
    }

    /**
     * @expectedException \Logic\Bank\Exception\Withdrawal\LimitAmountPerDayExceeded
     */
    public function testInvalidWithdrawalTransactionsPerDay()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::deposit(40000);
        Bank::deposit(40000);
        Bank::deposit(40000);

        Bank::withdraw(20000);
        Bank::withdraw(20000);
        Bank::withdraw(20000);
    }

    /**
     * @expectedException \Logic\Bank\Exception\Withdrawal\InsufficientBalance
     */
    public function testInvalidWithdrawalInsufficient()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::withdraw(13.37);
    }

    /**
     * @expectedException \Logic\Bank\Exception\NonPositiveAmount
     */
    public function testNegativeWithdrawalAmount()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::withdraw(-1);
    }

    /**
     * @expectedException \Logic\Bank\Exception\NonPositiveAmount
     */
    public function testZeroWithdrawalAmount()
    {
        $this->assertEquals(0, Bank::getBalance());

        Bank::withdraw(0);
    }
}
