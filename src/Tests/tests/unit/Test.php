<?php

declare(strict_types=1);

abstract class Test extends \Codeception\Test\Unit
{
    protected function reloadDB()
    {
        \Application::$Instance->getDB()->exec(file_get_contents(__DIR__ . '/../_data/dump.sql'));
    }

    protected function _before()
    {
        $this->reloadDB();
    }
}
